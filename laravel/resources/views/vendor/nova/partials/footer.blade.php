<p class="mt-8 text-center text-xs text-80">
    <a href="mailto:sauterelle.corp@gmail.com" class="text-primary dim no-underline">Contactez-nous</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Sauterelle Corp - Par Sylvain Langer, Jocelyn Mahaud, Julien Hartzer, Robyn Danglos & Alhan Hilaire.
</p>

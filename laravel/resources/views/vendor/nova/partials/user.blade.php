<dropdown-trigger class="h-9 flex items-center">
    @isset($user->email)
        @if (is_null($user->avatar_url))
        <img
            src="https://secure.gravatar.com/avatar/{{ md5(\Illuminate\Support\Str::lower($user->email)) }}?size=512"
            class="rounded-full w-8 h-8 mr-3"
        />
        @else
        <img
            src="{{$user->avatar_url}}?size=512"
            class="rounded-full w-8 h-8 mr-3"
        />
        @endif
    @endisset

    <span class="text-90">
        {{ $user->email ?? __('Nova User') }}
    </span>
</dropdown-trigger>

<dropdown-menu slot="menu" width="200" direction="rtl">
    <ul class="list-reset">
        <li>
            <router-link :to="{
                name: 'detail',
                params: {
                    resourceName: 'users',
                    resourceId: '{{ $user->id }}'
                }
            }" class="block no-underline text-90 hover:bg-30 p-3">
            Profil
            </router-link>
        </li>
        <li>
            <a class="block no-underline text-90 hover:bg-30 p-3" href="mailto:sauterelle.corp@gmail.com">Assistance</a>
        </li>
        <li>
            <a href="{{ route('nova.logout') }}" class="block no-underline text-90 hover:bg-30 p-3">
                {{ __('Logout') }}
            </a>
        </li>
        <li>
            <nova-dark-theme-toggle
                label="{{ __('Thème sombre') }}"
            ></nova-dark-theme-toggle>
        </li>
    </ul>
</dropdown-menu>

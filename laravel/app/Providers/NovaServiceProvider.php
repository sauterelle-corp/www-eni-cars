<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Cards\Help;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;

use App\User;
use Illuminate\Http\Request;

use App\Policies\PermissionPolicy;
use App\Policies\RolePolicy;

use App\Nova\Metrics\MyReservations;
use App\Nova\Metrics\LastReservations;
use App\Nova\Metrics\LastTrips;

use Jubeki\Nova\Cards\Linkable\Linkable;
use Jubeki\Nova\Cards\Linkable\LinkableAway;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Nova::createUserUsing(function ($command) {
            return [
                $command->ask('Lastname'),
                $command->ask('Firstname'),
                $command->ask('Email'),
                $command->secret('Password'),
            ];
        }, function ($lastname, $firstname, $email, $password) {
            (new User)->forceFill([
                'lastname' => $lastname,
                'firstname' => $firstname,
                'email' => $email,
                'password' => bcrypt($password),
            ])->save();
        });

        Nova::userTimezone(function (Request $request) {
            // app.timezone = 'UTC'
            return 'Europe/Paris';
        });
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        // Gate::define('viewNova', function ($user) {
        //     return in_array($user->email, [
        //         //
        //     ]);
        // });

        Gate::define('viewNova', function ($user) {
            if (!$user->isActive()) {
                abort(redirect('/')->with('warning', 'You do not have permission to access this page!'));
            }
            return $user->is_active;
        });
    }

    /**
     * Get the cards that should be displayed on the default Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            (new Linkable)
                ->title('Bienvenue ' . auth()->user()->firstname)
                ->url("/resources/users/" . auth()->user()->id)
                ->subtitle('Mon profil')
                ->width('1/3'),
            (new \ChrisWare\NovaClockCard\NovaClockCard)
                ->locale('fr')
                ->dateFormat('dddd, Do MMMM YYYY')
                ->timeFormat('LTS')
                ->timezone('GMT+1')
                ->width('1/3'),
            (new LinkableAway)
                ->title('Météo France ☀️')
                ->subtitle('https://meteofrance.com/')
                ->url("https://meteofrance.com/")
                ->width('1/3'),
            (new MyReservations)->canSee(function ($request) {
                return \Auth::user()->hasPermissionTo('view any MyReservations');
            }),
            (new LastReservations)->canSee(function ($request) {
                return \Auth::user()->hasPermissionTo('view any LastReservations');
            }),
            (new LastTrips),
        ];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards()
    {
        return [];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [
            \Vyuldashev\NovaPermission\NovaPermissionTool::make()
                ->rolePolicy(RolePolicy::class)
                ->permissionPolicy(PermissionPolicy::class),
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Nova::sortResourcesBy(function ($resource) {
            return $resource::$priority ?? 99999;
        });
    }
}

<?php

namespace App\Providers;

use App\Brand;
use App\Car;
use App\Key;
use App\Reservation;
use App\Site;
use App\SiteType;
use App\Trip;
use App\Type;
use App\User;

use App\Policies\BrandPolicy;
use App\Policies\CarPolicy;
use App\Policies\KeyPolicy;
use App\Policies\ReservationPolicy;
use App\Policies\SitePolicy;
use App\Policies\SiteTypePolicy;
use App\Policies\TripPolicy;
use App\Policies\TypePolicy;
use App\Policies\UserPolicy;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Brand::class => BrandPolicy::class,
        Car::class => CarPolicy::class,
        Key::class => KeyPolicy::class,
        Reservation::class => ReservationPolicy::class,
        Site::class => SitePolicy::class,
        SiteType::class => SiteTypePolicy::class,
        Trip::class => TripPolicy::class,
        Type::class => TypePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Implicitly grant "Super ADMIN" role all permissions
        // This works in the app by using gate-related functions like auth()->user->can() and @can()
        Gate::before(function ($user, $ability) {
            return $user->hasRole('super-admin') ? true : null;
        });
    }
}

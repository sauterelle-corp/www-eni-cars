<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'sites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function cars()
    {
        return $this->hasMany(Car::class);
    }

    public function keys()
    {
        return $this->hasMany(Key::class);
    }

    public function site_type()
    {
        return $this->belongsTo(SiteType::class);
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }
}

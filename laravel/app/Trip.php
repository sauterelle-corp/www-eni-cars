<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $table = 'trips';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_id', 'car_id', 'reservation_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'date_departure' => 'datetime',
        'date_arrival' => 'datetime',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_trips', 'trip_id', 'user_id');
    }

    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }

    public function departure()
    {
        return $this->belongsTo(Site::class, 'site_departure_id');
    }

    public function arrival()
    {
        return $this->belongsTo(Site::class, 'site_arrival_id');
    }
}

<?php

namespace App\Http\Controllers;

use App\SiteType;
use Illuminate\Http\Request;

class SiteTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SiteType  $siteType
     * @return \Illuminate\Http\Response
     */
    public function show(SiteType $siteType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SiteType  $siteType
     * @return \Illuminate\Http\Response
     */
    public function edit(SiteType $siteType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SiteType  $siteType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SiteType $siteType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SiteType  $siteType
     * @return \Illuminate\Http\Response
     */
    public function destroy(SiteType $siteType)
    {
        //
    }
}

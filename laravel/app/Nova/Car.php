<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Country;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Manmohanjit\BelongsToDependency\BelongsToDependency;
use Titasgailius\SearchRelations\SearchesRelations;
use Wemersonrv\InputMask\InputMask;

class Car extends Resource
{
    use SearchesRelations;

    public static $priority = 1;

    public static $group = 'Gestion véhicules';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Car::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'numberplate';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'numberplate',
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'brand' => ['name'],
    ];

    /**
     * The default sort associated with the resource.
     *
     * @var string
     */
    public static $defaultSort = [
        'numberplate' => 'asc'
    ];

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->numberplate;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('cars');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('car');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Image::make(__('img_path'), 'img_path'), // parametrer le patch d'accès - S3 COMPATIBLE ? ou alors local ?

            InputMask::make(__('numberplate'), 'numberplate')
                ->mask('XX-XXX-XX')
                ->rules('required', function ($attribute, $value, $fail) {
                    if (strtoupper($value) !== $value) {
                        return $fail('La plaque d\'immatriculation doit être en majuscule.');
                    }
                })
                ->resolveUsing(function ($numberplate) {
                    return strtoupper($numberplate);
                }),

            Country::make(__('country'), 'country')
                ->sortable()
                ->rules('required', 'max:2'),

            InputMask::make(__('county'), 'county')
                ->mask('##')
                ->rules('required'),

            BelongsTo::make(__('brand'), 'brand', Brand::class)
                ->sortable()
                ->rules('required'),

            BelongsToDependency::make(__('type'), 'type', Type::class)
                ->dependsOn('brand', 'brand_id')
                ->sortable()
                ->rules('required'),

            Number::make(__('place'), 'place')
                ->sortable()
                ->rules('required', 'max:2')
                ->hideFromIndex(),

            Select::make(__('auto_manual'), 'auto_manual')->options([
                'A' => 'Auto',
                'M' => 'Manual',
            ])
                ->rules('required')
                ->hideFromIndex(),

            BelongsTo::make(__('site'), 'site', Site::class)
                ->sortable()
                ->rules('required'),

            Textarea::make(__('other_information'), 'other_information')
                ->rules('max:255')
                ->hideFromIndex(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

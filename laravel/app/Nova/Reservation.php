<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Http\Requests\NovaRequest;
use Titasgailius\SearchRelations\SearchesRelations;

class Reservation extends Resource
{
    use SearchesRelations;

    public static $group = 'Gestion réservations';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Reservation::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'reservation';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'user' => ['lastname', 'firstname'],
    ];

    /**
     * The default sort associated with the resource.
     *
     * @var string
     */
    public static $defaultSort = [
        'created_at' => 'desc'
    ];

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->name;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('reservations');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('reservation');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make(__('name'), 'name')
                ->withMeta(['extraAttributes' => [
                    'readonly' => true
                ]])
                ->default(auth()->user()->firstname . ' ' . auth()->user()->lastname . ' ' . date(now()->format('d-m-Y H:i:s')))
                ->sortable()
                ->rules('required', 'max:255'),

            BelongsTo::make(__('user'), 'user', User::class) // récupération de l'utilisateur connecté
                ->withMeta(['extraAttributes' => [
                    'readonly' => true
                ]])
                ->withMeta([
                    'belongsToId' => $this->user_id ?? auth()->user()->id
                ])
                ->rules('required'),

            // BelongsTo::make(__('trips'), 'trips', Trip::class)
            //     ->sortable()
            //     ->rules('required'),

            HasMany::make(__('trips'), 'trips', Trip::class)
                ->sortable()
                ->rules('required'),

            Select::make(__('status'), 'status')->options([
                'En attente' => 'En attente',
                'Validé' => 'Validé',
                'Refusé' => 'Refusé',
                'Complété' => 'Complété',
            ])
                ->rules('required')
                ->hideFromIndex()
                ->onlyOnForms()
                ->default('En attente')
                ->withMeta(['extraAttributes' => [
                    'readonly' => \Auth::user()->hasPermissionTo('readonly') ? true : false
                ]]),

            Status::make(__('status'), 'status')
                ->loadingWhen(['En attente'])
                ->failedWhen(['Refusé']),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Fields\BelongsTo;
use Dniccum\PhoneNumber\PhoneNumber;
use EmilianoTisato\GoogleAutocomplete\GoogleAutocomplete;

class Site extends Resource
{
    public static $group = 'Gestion sites';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Site::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name', 'address',
    ];

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->name;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('sites');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('site');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make(__('name'), 'name')
                ->sortable()
                ->rules('required'),

            GoogleAutocomplete::make(__('address'), 'address')
                ->countries('FR')
                ->rules('required'),

            PhoneNumber::make(__('phonenumber'), 'phonenumber')
                ->format('+33 (#)# ## ## ## ##')
                ->country('FR')
                ->linkOnIndex()
                ->linkOnDetail()
                ->rules('required'),

            Textarea::make(__('other_information'), 'other_information')
                ->rules('max:255')
                ->hideFromIndex(),

            BelongsTo::make(__('site_type'), 'site_type', SiteType::class)
                ->sortable()
                ->rules('required'),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

<?php

namespace App\Nova;

use App\UserTrip;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\Status;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Select;
use Manmohanjit\BelongsToDependency\BelongsToDependency;
use Orlyapps\NovaBelongsToDepend\NovaBelongsToDepend;
use Laravel\Nova\Http\Requests\NovaRequest;
use Titasgailius\SearchRelations\SearchesRelations;
use Benjacho\BelongsToManyField\BelongsToManyField;

class Trip extends Resource
{
    use SearchesRelations;

    public static $group = 'Gestion réservations';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Trip::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'trip';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * The relationship columns that should be searched.
     *
     * @var array
     */
    public static $searchRelations = [
        'users' => ['lastname', 'firstname'],
        'car' => ['numberplate'],
        'reservation' => ['name'],
    ];

    /**
     * The default sort associated with the resource.
     *
     * @var string
     */
    public static $defaultSort = [
        'date_departure' => 'desc'
    ];

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->name;
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return __('trips');
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return __('trip');
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make(__('title'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            NovaBelongsToDepend::make(__('departure'), 'departure', Site::class)
                ->placeholder('Site de départ')
                ->options(\App\Site::all()),

            NovaBelongsToDepend::make(__('arrival'), 'arrival', Site::class)
                ->placeholder('Site d\'arrivée')
                ->options(\App\Site::all()),

            DateTime::make(__('date_departure'), 'date_departure')
                ->format('DD-MM-YYYY HH:mm:ss') // https://momentjs.com/docs/#/parsing/string-format/
                ->firstDayOfWeek(1)
                ->sortable()
                ->rules('required'),

            DateTime::make(__('date_arrival'), 'date_arrival')
                ->format('DD-MM-YYYY HH:mm:ss') // https://momentjs.com/docs/#/parsing/string-format/
                ->firstDayOfWeek(1)
                ->sortable()
                ->rules('required'),

            BelongsToManyField::make(__('carpool'), 'users', User::class)
                ->sortable()
                ->rules('required'),

            BelongsTo::make(__('car'), 'car', Car::class)
                ->sortable()
                ->rules('required'),

            BelongsTo::make(__('reservation'), 'reservation', Reservation::class)
                ->sortable()
                ->rules('required'),

            Select::make(__('status'), 'status')->options([
                'En attente' => 'En attente',
                'En cours' => 'En cours',
                'Terminé' => 'Terminé',
                'Annulé' => 'Annulé',
            ])
                ->rules('required')
                ->hideFromIndex()
                ->onlyOnForms()
                ->default('En attente')
                ->withMeta(['extraAttributes' => [
                    'readonly' => \Auth::user()->hasPermissionTo('readonly') ? true : false
                ]]),

            Status::make('Status')
                ->loadingWhen(['En attente'])
                ->failedWhen(['Annulé']),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}

<?php

namespace App\Nova\Metrics;

use App\Reservation;
use ThijsSimonis\NovaListCard\NovaListCard;

class LastReservations extends NovaListCard
{
    public $width = 'full';

    public function __construct()
    {
        parent::__construct();

        $this->rows(Reservation::select(['id', 'name', 'status'])
            ->where('status', '=', 'En attente')
            ->orderBy('created_at', 'DESC')
            ->limit(10)->get()
            ->map(
                function ($row) {
                    $row['view'] = config('nova.url') . '/resources/reservations/' . $row['id'];
                    return $row;
                }
            ));
    }

    public function uriKey(): string
    {
        return 'last-reservations';
    }
}

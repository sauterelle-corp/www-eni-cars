<?php

namespace App\Nova\Metrics;

use App\Reservation;
use ThijsSimonis\NovaListCard\NovaListCard;

class MyReservations extends NovaListCard
{
    public $width = '1/2';

    public function __construct()
    {
        parent::__construct();

        $this->rows(Reservation::select(['id', 'name', 'status'])
            ->where('user_id', '=', auth()->user()->id)
            ->orderBy('created_at', 'DESC')
            ->limit(10)->get()
            ->map(
                function ($row) {
                    $row['view'] = config('nova.url') . '/resources/reservations/' . $row['id'];
                    return $row;
                }
            ));
    }

    public function uriKey(): string
    {
        return 'my-reservations';
    }
}

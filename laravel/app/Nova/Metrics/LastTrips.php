<?php

namespace App\Nova\Metrics;

use App\Trip;
use ThijsSimonis\NovaListCard\NovaListCard;

class LastTrips extends NovaListCard
{
    public $width = '1/2';

    public function __construct()
    {
        parent::__construct();

        $this->rows(Trip::select(['trips.id', 'trips.name', 'cars.numberplate'])
            ->orderBy('trips.created_at', 'DESC')
            ->join('cars', 'cars.id', '=', 'trips.car_id')
            ->limit(10)->get()
            ->map(
                function ($row) {
                    $row['view'] = config('nova.url') . '/resources/trips/' . $row['id'];
                    return $row;
                }
            ));
    }

    public function uriKey(): string
    {
        return 'last-trips';
    }
}

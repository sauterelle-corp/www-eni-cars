<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('site_departure_id');
            $table->integer('site_arrival_id');
            $table->timestamp('date_departure');
            $table->timestamp('date_arrival')->nullable();
            $table->integer('car_id')->nullable();
            $table->integer('reservation_id');
            $table->string('status'); // non débuté - en cours - terminé - annulé
            // $table->boolean('is_validate'); // l'admin a t'il validé 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}

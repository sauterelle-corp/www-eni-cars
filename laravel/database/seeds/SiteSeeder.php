<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sites')->insert([
            'name' => 'ENI Nantes Franklin',
            'address' => 'Zac Moulin Neuf r Benjamin Franklin, 44800 Saint-Herblain',
            'phonenumber' => '02 28 03 17 28',
            'other_information' => '',
            'site_type_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('sites')->insert([
            'name' => 'ENI Nantes Faraday',
            'address' => '3 Rue Michael Faraday, 44800 Saint-Herblain',
            'phonenumber' => '02 28 03 17 28',
            'other_information' => '',
            'site_type_id' => '2',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('sites')->insert([
            'name' => 'ENI Niort',
            'address' => '19 Av. Léo Lagrange Bâtiment B et C, 79000 Niort',
            'phonenumber' => '05 35 37 11 83',
            'other_information' => '',
            'site_type_id' => '2',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('sites')->insert([
            'name' => 'ENI Rennes',
            'address' => 'ZAC de, La Conterie, 8 Rue Léo Lagrange, 35131 Chartres-de-Bretagne',
            'phonenumber' => '02 23 30 18 50',
            'other_information' => '',
            'site_type_id' => '2',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('sites')->insert([
            'name' => 'ENI Quimper',
            'address' => '2 Rue Georges Perros, 29000 Quimper',
            'phonenumber' => '02 98 50 55 13',
            'other_information' => '',
            'site_type_id' => '2',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class ReservationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservations')->insert([
            'name' => 'Stéphane Gobin 04-10-2021 21:30:00',
            'user_id' => '13',
            'status' => 'En attente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('reservations')->insert([
            'name' => 'Stéphane Gobin 04-10-2021 21:30:00',
            'user_id' => '13',
            'status' => 'En attente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('reservations')->insert([
            'name' => 'Denis Sanchez 05-10-2021 09:00:00',
            'user_id' => '27',
            'status' => 'Complété',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('reservations')->insert([
            'name' => 'Sophie Doré 04-10-2021 09:00:00',
            'user_id' => '1',
            'status' => 'Complété',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('reservations')->insert([
            'name' => 'Vincent David 11-10-2021 14:00:00',
            'user_id' => '2',
            'status' => 'Refusé',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}

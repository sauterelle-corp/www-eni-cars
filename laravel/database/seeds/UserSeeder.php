<?php

use Carbon\Carbon;

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'lastname' => 'ARTU',
            'firstname' => 'Sébastien',
            'email' => 'sartu@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'lastname' => 'BAILLE',
            'firstname' => 'Anne-lise',
            'email' => 'abaille@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 35 ; Rennes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'lastname' => 'BOUVET',
            'firstname' => 'Laurent',
            'email' => 'lbouvet@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'lastname' => 'BROSSIER',
            'firstname' => 'Gilles',
            'email' => 'gbrossier@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'lastname' => 'CHIRON',
            'firstname' => 'Olivier',
            'email' => 'ochiron@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'lastname' => 'CLAVEAU',
            'firstname' => 'Mathieu',
            'email' => 'mclaveau@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 35 ; Rennes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('users')->insert([
            'lastname' => 'DELACHESNAIS',
            'firstname' => 'Frédéric',
            'email' => 'fdelachesnais@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 35 ; Rennes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'DENIS',
            'firstname' => 'Mathieu',
            'email' => 'mdenis@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 35 ; Rennes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'FENDER',
            'firstname' => 'Clément',
            'email' => 'cfender@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'GAUTIER',
            'firstname' => 'Romain',
            'email' => 'rgautier@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 35 ; Rennes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'GOBIN',
            'firstname' => 'Stéphane',
            'email' => 'sgobin@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'GRELIER',
            'firstname' => 'François',
            'email' => 'fgrelier@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'GROHAN',
            'firstname' => 'Yann',
            'email' => 'ygrohan@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'GROUSSARD',
            'firstname' => 'Thierry',
            'email' => 'tgroussard@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 79 ; Niort',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'GUYADER',
            'firstname' => 'Thélo',
            'email' => 'tguyader@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 35 ; Rennes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'LARGEAU',
            'firstname' => 'Thierry',
            'email' => 'tlargeau@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'LE LOUARN',
            'firstname' => 'Mathieu',
            'email' => 'mlelouarn@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'LEBOUCHER',
            'firstname' => 'Sébastien',
            'email' => 'sleboucher@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'MARIAULT',
            'firstname' => 'Anthony',
            'email' => 'amariault@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 79 ; Niort',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'MARSAUDON',
            'firstname' => 'François',
            'email' => 'fmarsaudon@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'MICHAUD',
            'firstname' => 'Grégory',
            'email' => 'gmichaud@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'MONTEMBAULT',
            'firstname' => 'Philippe',
            'email' => 'pmontembault@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 35 ; Rennes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'RICHEZ',
            'firstname' => 'Cédric',
            'email' => 'crichez@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 29 ; Quimper',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'ROUSSEL',
            'firstname' => 'Romain',
            'email' => 'rroussel@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 35 ; Rennes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'SANCHEZ',
            'firstname' => 'Denis',
            'email' => 'dsanchez@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 35 ; Rennes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'THOMAS-LESNE',
            'firstname' => 'Stéphane',
            'email' => 'sthomas@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 29 ; Quimper',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'THOUIN',
            'firstname' => 'Frédéric',
            'email' => 'fthouin@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'TRILLARD',
            'firstname' => 'Julien',
            'email' => 'jtrillard@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'TROPÉE',
            'firstname' => 'Sylvain',
            'email' => 'stropee@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Développement ; Site : 35 ; Rennes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([
            'lastname' => 'WAWRZYNIAK',
            'firstname' => 'François-Xavier',
            'email' => 'fxwawrzyniak@campus-eni.fr',
            'password' => Hash::make('password'),
            'other_information' => 'Équipe : Réseau ; Site : 44 ; Nantes',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TripSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trips')->insert([
            'name' => 'NANTES FRANKLIN - NIORT',
            'site_departure_id' => '1',
            'site_arrival_id' => '3',
            'date_departure' => Carbon::now()->format('Y-m-d H:i:s'),
            'reservation_id' => '1',
            'status' => 'En cours',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('trips')->insert([
            'name' => 'NANTES FARADAY - QUIMPER',
            'site_departure_id' => '2',
            'site_arrival_id' => '5',
            'date_departure' => Carbon::now()->format('Y-m-d H:i:s'),
            'date_arrival' => Carbon::now()->format('Y-m-d H:i:s'),
            'car_id' => '1',
            'reservation_id' => '1',
            'status' => 'En attente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('trips')->insert([
            'name' => 'NIORT - NANTES FARADAY',
            'site_departure_id' => '3',
            'site_arrival_id' => '1',
            'date_departure' => Carbon::now()->format('Y-m-d H:i:s'),
            'date_arrival' => Carbon::now()->format('Y-m-d H:i:s'),
            'car_id' => '2',
            'reservation_id' => '1',
            'status' => 'En attente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('trips')->insert([
            'name' => 'QUIMPER - NANTES FARADAY',
            'site_departure_id' => '5',
            'site_arrival_id' => '2',
            'date_departure' => Carbon::now()->format('Y-m-d H:i:s'),
            'date_arrival' => Carbon::now()->format('Y-m-d H:i:s'),
            'car_id' => '1',
            'reservation_id' => '1',
            'status' => 'En attente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('trips')->insert([
            'name' => 'RENNES - NIORT',
            'site_departure_id' => '4',
            'site_arrival_id' => '3',
            'date_departure' => Carbon::now()->format('Y-m-d H:i:s'),
            'date_arrival' => Carbon::now()->format('Y-m-d H:i:s'),
            'car_id' => '3',
            'reservation_id' => '2',
            'status' => 'En attente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('trips')->insert([
            'name' => 'NIORT - RENNES',
            'site_departure_id' => '3',
            'site_arrival_id' => '4',
            'date_departure' => Carbon::now()->format('Y-m-d H:i:s'),
            'date_arrival' => Carbon::now()->format('Y-m-d H:i:s'),
            'car_id' => '3',
            'reservation_id' => '2',
            'status' => 'En attente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('trips')->insert([
            'name' => 'RENNES - QUIMPER',
            'site_departure_id' => '4',
            'site_arrival_id' => '5',
            'date_departure' => Carbon::now()->format('Y-m-d H:i:s'),
            'date_arrival' => Carbon::now()->format('Y-m-d H:i:s'),
            'reservation_id' => '3',
            'status' => 'Annulé',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('trips')->insert([
            'name' => 'NANTES FRANKLIN - NIORT',
            'site_departure_id' => '1',
            'site_arrival_id' => '3',
            'date_departure' => Carbon::now()->format('Y-m-d H:i:s'),
            'reservation_id' => '4',
            'car_id' => '1',
            'status' => 'En attente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('trips')->insert([
            'name' => 'NIORT - NANTES FARADAY',
            'site_departure_id' => '3',
            'site_arrival_id' => '1',
            'date_departure' => Carbon::now()->format('Y-m-d H:i:s'),
            'date_arrival' => Carbon::now()->format('Y-m-d H:i:s'),
            'car_id' => '1',
            'reservation_id' => '4',
            'status' => 'En attente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('trips')->insert([
            'name' => 'NANTES FARADAY - NIORT',
            'site_departure_id' => '1',
            'site_arrival_id' => '3',
            'date_departure' => Carbon::now()->format('Y-m-d H:i:s'),
            'date_arrival' => Carbon::now()->format('Y-m-d H:i:s'),
            'reservation_id' => '5',
            'status' => 'Annulé',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}

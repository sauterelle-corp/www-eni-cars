<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class KeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('keys')->insert([
            'name' => 'Clé Renault Clio',
            'site_id' => '1', // site où sont entreposées les clées
            'site_bis_id' => '2', // site où sont entreposées les doubles de clées
            'car_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('keys')->insert([
            'name' => 'Clé Peugeot 208',
            'site_id' => '1', // site où sont entreposées les clées
            'site_bis_id' => '1', // site où sont entreposées les doubles de clées
            'car_id' => '2',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}

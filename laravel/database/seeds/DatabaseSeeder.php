<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesAndPermissionsSeeder::class,
            UserSeeder::class,
            BrandSeeder::class,
            TypeSeeder::class,
            SiteTypeSeeder::class,
            SiteSeeder::class,
            CarSeeder::class,
            KeySeeder::class,
            ReservationSeeder::class,
            TripSeeder::class,
        ]);

    }
}

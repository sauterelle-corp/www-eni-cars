<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Support\Str;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Create permissions
        Permission::create(['name' => 'all access granted on users']);
        Permission::create(['name' => 'all access granted on brands']);
        Permission::create(['name' => 'all access granted on brand types']);
        Permission::create(['name' => 'all access granted on cars']);
        Permission::create(['name' => 'all access granted on keys']);
        Permission::create(['name' => 'all access granted on site types']);
        Permission::create(['name' => 'all access granted on sites']);
        Permission::create(['name' => 'all access granted on reservations']);
        Permission::create(['name' => 'all access granted on trips']);

        // User permissions
        Permission::create(['name' => 'view any users']);
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'edit all users']);

        // Brand permissions
        Permission::create(['name' => 'view any brands']);
        Permission::create(['name' => 'view brands']);

        // Brand Type permissions
        Permission::create(['name' => 'view any brand types']);
        Permission::create(['name' => 'view brand types']);

        // Car permissions
        Permission::create(['name' => 'view any cars']);
        Permission::create(['name' => 'view cars']);

        // Key permissions
        Permission::create(['name' => 'view any keys']);
        Permission::create(['name' => 'view keys']);

        // Site Type permissions
        Permission::create(['name' => 'view any site types']);
        Permission::create(['name' => 'view site types']);

        // Site permissions
        Permission::create(['name' => 'view any sites']);
        Permission::create(['name' => 'view sites']);

        // Reservations permissions
        Permission::create(['name' => 'view any reservations']);
        Permission::create(['name' => 'view reservations']);

        // Trips permissions
        Permission::create(['name' => 'view any trips']);
        Permission::create(['name' => 'view trips']);


        // Dashboard metrics packages permissions
        Permission::create(['name' => 'view any MyReservations']);
        Permission::create(['name' => 'view any LastReservations']);

        // External packages permissions

        // readonly
        Permission::create(['name' => 'readonly']);

        
        // Create roles and assign created permissions
        $role2 = Role::create(['name' => 'employee'])->givePermissionTo([
            'all access granted on reservations',
            'all access granted on trips',

            'view cars',
            'view any cars',

            'view any users',
            'view users',

            'view any sites',
            'view sites',

            'view any MyReservations',

            'readonly',
        ]);

        $role1 = Role::create(['name' => 'super-admin'])->givePermissionTo([
            'view any MyReservations',
            'view any LastReservations',
        ]);

        // Create users and assign created roles
        $user = Factory(App\User::class)->create([
            'lastname' => 'DORÉ',
            'firstname' => 'Sophie',
            'email' => 'sdore@eni-ecole.fr',
            'phonenumber' => '+33 (0)2 28 03 17 28',
            'password' => Hash::make('password'),
            'other_information' => Str::random(255),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        
        $user->assignRole($role1);

        $user2 = Factory(App\User::class)->create([
            'lastname' => 'DAVID',
            'firstname' => 'Vincent',
            'email' => 'vdavid@campus-eni.fr',
            'phonenumber' => '+33 (0)2 23 30 18 50',
            'password' => Hash::make('password'),
            'other_information' => Str::random(255),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        
        $user2->assignRole($role2);
    }
}

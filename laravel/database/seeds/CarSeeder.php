<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
            'numberplate' => 'AB-123-CD',
            'country' => 'FR',
            'county' => '44',
            'place' => '5',
            'auto_manual' => 'M',
            'other_information' => 'Contrôle technique à faire 12/21',
            'brand_id' => '2',
            'type_id' => '2',
            'site_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('cars')->insert([
            'numberplate' => 'EF-123-GH',
            'country' => 'FR',
            'county' => '44',
            'place' => '2',
            'auto_manual' => 'M',
            'other_information' => 'Contrôle technique à faire 10/21',
            'brand_id' => '2',
            'type_id' => '2',
            'site_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('cars')->insert([
            'numberplate' => 'IJ-123-KL',
            'country' => 'FR',
            'county' => '44',
            'place' => '2',
            'auto_manual' => 'M',
            'other_information' => 'Petite raillure sur la portière droite',
            'brand_id' => '1',
            'type_id' => '1',
            'site_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('cars')->insert([
            'numberplate' => 'MN-123-OP',
            'country' => 'FR',
            'county' => '44',
            'place' => '2',
            'auto_manual' => 'M',
            'other_information' => '',
            'brand_id' => '2',
            'type_id' => '3',
            'site_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('cars')->insert([
            'numberplate' => 'QR-123-ST',
            'country' => 'FR',
            'county' => '44',
            'place' => '2',
            'auto_manual' => 'M',
            'other_information' => '',
            'brand_id' => '2',
            'type_id' => '3',
            'site_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('cars')->insert([
            'numberplate' => 'UV-123-WX',
            'country' => 'FR',
            'county' => '35',
            'place' => '2',
            'auto_manual' => 'M',
            'other_information' => '',
            'brand_id' => '2',
            'type_id' => '2',
            'site_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        DB::table('cars')->insert([
            'numberplate' => 'YZ-123-AB',
            'country' => 'FR',
            'county' => '35',
            'place' => '2',
            'auto_manual' => 'M',
            'other_information' => 'Petite raillure sur la portière droite',
            'brand_id' => '1',
            'type_id' => '1',
            'site_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}

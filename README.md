## Laravel Application Settings
```shell
$ cp ./laravel/.env.example ./laravel/.env
$ vi ./laravel/.env
```

## Laravel env file should contain :
```shell
APP_NAME=www-eni-cars
APP_ENV=dev
APP_KEY=base64:dfEXIcBJB69zJCPKqIKlSuxpzRdjL3dj4iqRn2tkYpE=
APP_DEBUG=true
APP_URL=http://localhost:8015

LOG_CHANNEL=stack

# Your email for Let's Encrypt register
LETSENCRYPT_EMAIL=sauterelle.corp@gmail.com

DB_CONNECTION=pgsql
DB_HOST=db-cars
DB_PORT=5432
DB_DATABASE=laravel
DB_USERNAME=laravel
DB_PASSWORD=password

BROADCAST_DRIVER=log
CACHE_DRIVER=file
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

ADDRESS_AUTOCOMPLETE_API_KEY=AIzaSyDuBXgASOQX19ZVlFXSvMMGDdTfK8CUvtk

#MAIL_MAILER=
#MAIL_HOST=
#MAIL_PORT=
#MAIL_USERNAME=
#MAIL_PASSWORD=
#MAIL_ENCRYPTION=
#MAIL_FROM_ADDRESS=
#MAIL_FROM_NAME=
```

## Docker Settings
```shell
$ cp .env.example .env
$ vi .env
```
## Docker env file should contain :
```shell
# Network name
# 
# Your container app must use a network connected to your webproxy 
# https://github.com/evertramos/docker-compose-letsencrypt-nginx-proxy-companion
#
NETWORK=webproxy
EXPOSED_PORT=8015

# Environment Tags
SERVICE_TAGS=dev

# Parameters APP image source
APP_IMG=registry.gitlab.com/sauterelle-corp/www-eni-cars
WEBSERVER_IMG=nginx:1.17.9-alpine
POSTGRES_IMG=postgres:11.10-alpine

# Path to store your database
# DB_PATH=/path/to/your/local/database/folder
DB_PATH=./run/db

# Root password for your database
POSTGRES_ROOT_PASSWORD=secret

# Database name, user and password for your Laravel App
POSTGRES_DATABASE=laravel
POSTGRES_USER=laravel
POSTGRES_PASSWORD=password

# Your domain (or domains)
DOMAINS=http://localhost:8015

# Your email for Let's Encrypt register
LETSENCRYPT_EMAIL=sauterelle.corp@gmail.com
```

## If necessary Create a Docker Network
```shell
$ docker network create webproxy
```

## Create Docker Containers
```shell
$ docker-compose up -d
```

## Browse in my app container (exemple with this project)
```shell
$ docker exec -it www-eni-cars bash
```

## Laravel Install (not mandatory when create-project is executed but do it after a git pull)
```shell
$ composer install
```

## Build my database from migrations + seeds
```shell
$ php artisan migrate:refresh --seed
```

## Create First Nova Admin (in app container)
```shell
$ php artisan nova:user
```
